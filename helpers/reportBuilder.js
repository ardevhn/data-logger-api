var _ = require('lodash');
var moment = require('moment');

var eventCodes = {
	'104': 'Engine load',
	'105': 'Engine coolant temperature',
	'10A': 'Fuel pressure',
	'10B': 'Intake manifold absolute pressure',
	'10C': 'Engine RPM',
	'10D': 'Vehicle speed',
	'10E': 'Timing advance',
	'10F': 'Intake air temperature',
	'110': 'MAF air flow rate',
	'111': 'Throttle position',
	'11F': 'Run time since engine start',
	'121': 'Distance traveled with malfunction indicator lamp',
	'12F': 'Fuel Level Input',
	'131': 'Distance traveled since codes cleared',
	'133': 'Barometric pressure',
	'142': 'Control module voltage',
	'143': 'Absolute load value',
	'15B': 'Hybrid battery pack remaining life',
	'15C': 'Engine oil temperature',
	'15E': 'Engine fuel rate',
	'11' : 'UTC Date',
	'10' : 'UTC Time',
	'A'  : 'Latitude',
	'B'  : 'Longitude',
	'C'  : 'Altitude (m)',
	'D'  : 'Speed',
	'E'  : 'Course',
	'F'  : 'Satellite number',
	'20' : 'Accelerometer data',
	'21' : 'Gyroscope data',
	'22' : 'Magitude field data',
	'23' : 'Device temperature',
	'24' : 'Battery voltage',
	'30' : 'Trip distance'
};

function copyByValue(value){
	return JSON.parse(JSON.stringify(value));
}

function finishBuildingReport(report, reportList){
	var newReport = copyByValue(report);
	if(newReport.speed == 0){
		newReport.status = "On Duty";
	}else if(newReport.speed > 0){
		newReport.status = "Driving";
	}

	
	reportList.push(newReport);
	report = {
		location: {type: 'Point', coordinates: [0, 0]}
	};
}


module.exports = {
	build: function(report, event, reportList, reportDate){

		var dataTypeCode = event[1].toUpperCase();

		switch(dataTypeCode){

			case '131':

				//Distance traveled since codes cleared
				break;
			case '10C':

				//Engine RPM
				if(report.engineRPM){
					finishBuildingReport(report, reportList);
					break;
				}
				report.engineRPM = parseInt(event[2])
				break;

			case '10D': 
				if(report.speed){
					finishBuildingReport(report, reportList);
					break;
				}
				report.speed = event[2];
				//Vehicle speed
				break;
			case '111': 
				//Throttle position
				break;
			case '104': 
				//Engine load
				break;
			case '24': 
				//Battery voltage (unit is 0.01V)
				break;
			case '20': 
				//Accelerometer data (x/y/z)
				break;
			case '105': 
				//Engine coolant temperature
				break;
			case '10F': 
				//Intake air temperature
				break;
			case '11': 
				//UTC Date (DDMMYY)
				break;
			case '10': 
				//UTC Time (HHMMSSmm)
				if(report.datetime){
					finishBuildingReport(report, reportList);
					break;
				}
				var reportRAWTime = event[2];
				reportRAWTime = reportDate + reportRAWTime;
				var newReportDateTime = moment.utc(reportRAWTime, 'MMDDHHmmssSS').toDate();
				report.datetime = newReportDateTime;
				break;
			case 'A': 
				//Latitude
				if(report.location.coordinates[1] !== 0){
					finishBuildingReport(report, reportList);
					break;
				}
				report.location.coordinates[1] = event[2];
				break;
			case 'B': 
				//Longitude
				if(report.location.coordinates[0] !== 0){
					finishBuildingReport(report, reportList);
					break;
				}
				report.location.coordinates[0] = event[2];
				break;
			case 'C': 
				//Altitude (m)
				break;
			case 'D': 
				//GPS Speed (km/h)
				break;
			case 'E': 
				//Course (degree)
				break;
			case 'F': 
				//Satellite number
				break;
			default:
				console.log("not recognized: ", dataTypeCode);
				break;
		}
		
	}
};