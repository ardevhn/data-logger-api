var mongoose = require('mongoose');  
var Schema = mongoose.Schema;  
var ReportSchema = new Schema({  
	status: { type: String, default: ''},
	speed: { type: Number, default: 0 },
	datetime: Date,
	engineRPM: { type: Number, default: 0 },
	latitude: { type: String },
	longitude: { type: String },
	location: { type: { type: String, enum: "Point", default: "Point" }, coordinates: { type: [Number], default: [0, 0] } },
},
{
  timestamps: true
});
var ReportModel = mongoose.model('Reports', ReportSchema); 