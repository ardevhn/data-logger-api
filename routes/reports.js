var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');
var util = require('util');
var csv = require("fast-csv");
var moment = require('moment');
var _ = require('lodash');
var mongoose = require('mongoose');
var Reports = mongoose.model('Reports');
var moment = require('moment');
var ReportBuilder = require('../helpers/reportBuilder');


router.post('/upload-log', function(req, res, next) {

  	var form = new multiparty.Form();
  	
    form.parse(req, function(err, fields, files) {

		var reportList = [];
		var reportTempContainer = {
			location: {type: 'Point', coordinates: [0, 0]}
		};
		var fileName = files.file[0].originalFilename;

		var reportDate = fileName.split('.')[0];
		reportDate = reportDate[0] + reportDate[1] + reportDate[2] + reportDate[3];
		// reportDate = moment(fileName, "MMDDHHmm");
		
		csv.fromPath(files.file[0].path, { headers: false, objectMode: true })
		 .on("data", function(event){
		 	/*
		 		Aqui se recibe cada linea del .csv como una lista de strings Ejemplo

		 			"#1679,131,766"  =>  event = ['#1679','131','766']
			*/
		 	var report = buildReport(reportTempContainer, event, reportList, reportDate);
		 	if(report.isReadyToPush){
		 		delete report.isReadyToPush;

		 		//convert report.datetime to Date Object because for some unknown reason it gets converted to another object
		 		report.datetime = new Date(report.datetime);

		 		reportList.push(report);
			 	reportTempContainer = {
					location: {type: 'Point', coordinates: [0, 0]}
				};
		 	}else{
		 		reportTempContainer = report;
		 	}
		 })
		 .on("end", function(){

		     console.log("Length: "+ reportList.length)

		     Reports.collection.insert(reportList, function(err, docs){
		     	if (err) {
			        res.status(500).send("error adding reports");
			    } else {
			        console.info('potatoes were successfully stored.', docs.insertedCount);
			        res.status(200).send("Reports successfully added");
			    }
		     });
		     // res.end(util.inspect({fields: fields, files: files}));
		 });

      
    });

    // return;
});

function copyByValue(value){
	return JSON.parse(JSON.stringify(value));
}

function finishBuildingReport(report, reportList){
	var newReport = copyByValue(report);

	if(newReport.speed === 0){
		newReport.status = "On Duty";
	}else if(newReport.speed > 0){
		newReport.status = "Driving";
	}

	newReport.isReadyToPush = true;
	return newReport;
}



function buildReport(report, event, reportList, reportDate){
	var _report = report;
	var dataTypeCode = event[1].toUpperCase();

	switch(dataTypeCode){

		case '131':

			//Distance traveled since codes cleared
			break;
		case '10C':

			//Engine RPM
			if(report.engineRPM){
				return finishBuildingReport(report, reportList);
				break;
			}
			report.engineRPM = parseInt(event[2])
			break;

		case '10D': 
			if(report.speed){
				return finishBuildingReport(report, reportList);
				break;
			}
			report.speed = parseInt(event[2]);
			//Vehicle speed
			break;
		case '111': 
			//Throttle position
			break;
		case '104': 
			//Engine load
			break;
		case '24': 
			//Battery voltage (unit is 0.01V)
			break;
		case '20': 
			//Accelerometer data (x/y/z)
			break;
		case '105': 
			//Engine coolant temperature
			break;
		case '10F': 
			//Intake air temperature
			break;
		case '11': 
			//UTC Date (DDMMYY)
			break;
		case '10': 
			//UTC Time (HHMMSSmm)
			if(report.datetime){
				return finishBuildingReport(report, reportList);
				break;
			}
			var reportRAWTime = event[2];
			reportRAWTime = reportDate + reportRAWTime;
			var newReportDateTime = moment.utc(reportRAWTime, 'MMDDHHmmssSS').toDate();
			newReportDateTime = new Date(newReportDateTime)
			
			report.datetime = newReportDateTime;
			break;
		case 'A': 
			//Latitude
			//if(report.location.coordinates[1] !== 0){
			if(report.latitude){
				return finishBuildingReport(report, reportList);
				break;
			}
			//report.location.coordinates[1] = event[2];
			report.latitude = event[2];
			break;
		case 'B': 
			//Longitude
			//if(report.location.coordinates[0] !== 0){
			if(report.longitude){
				return finishBuildingReport(report, reportList);
				break;
			}
			//report.location.coordinates[0] = event[2];
			report.longitude = event[2];

			break;
		case 'C': 
			//Altitude (m)
			break;
		case 'D': 
			//GPS Speed (km/h)
			break;
		case 'E': 
			//Course (degree)
			break;
		case 'F': 
			//Satellite number
			break;
		default:
			console.log("not recognized: ", dataTypeCode);
			break;
	}

	return _report;
	
}


router.get('/:date', function(req, res, next) {
	var initDate = req.params.date; //MM-DD-YYYY
	initDate = moment(initDate,'MM-DD-YYYY').toDate();

	var tomorrow = new Date(initDate);
    tomorrow.setDate(tomorrow.getDate()+1);


	Reports.find({ datetime: { $gte: initDate, $lt: tomorrow }}).sort({datetime: 1}).exec(function(err, docs){
		if(err) return res.status(500).send("Error fetching reports");

		var reports = [];

        _.forEach(docs, function(report) {
            if (_.isEmpty(reports) === true) {
                var firstReportToAdd = {
                    status : report.status,
                    started_at: report.datetime,
                    finished_at: '',
                    duration: 0,
                    location: '',
					id: 1
                };
                reports.push(firstReportToAdd);
            } else {
                var lastReport = _.last(reports);
                if (lastReport.status !== report.status) {
                    var newReportToAdd = {
                        status : report.status,
                        started_at: report.datetime,
                        finished_at: '',
                        location: '',
                        duration: 0,
						id: lastReport.id + 1
                    };
                    reports.push(newReportToAdd);
                } else {
                    var index = _.indexOf(reports, _.find(reports, {id: lastReport.id}));
                    lastReport.finished_at = report.datetime;
                    lastReport.duration =  moment(lastReport.finished_at).diff(moment(lastReport.started_at), "seconds");
                    reports.splice(index, 1, lastReport);
                }
            }
        });

		res.status(200).send(reports);
	})

	// Reports.aggregate([
 //      	{ $match: { datetime: { $gte: initDate, $lt: tomorrow }}},
 //      	{ $group : { _id : "$status", reports: { $push: "$$ROOT" } } },
 //      	{ $sort: { "datetime": -1 } },
 //  	],
 //    function(err, result) {
 //    	console.log("END SEARCHING")
 //    	console.log("results: ", result)
 //       res.status(200).send(result);
 //    })

});

module.exports = router;
